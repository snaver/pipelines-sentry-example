# Pipelines Sentry Example

This tutorial will detail how you can setup Bitbucket [Pipelines](https://bitbucket.org/product/features/pipelines) to talk to [Sentry.io](https://sentry.io/) to create [Releases](https://docs.sentry.io/learn/releases/) and trigger [Deploys](https://blog.sentry.io/2017/05/09/release-deploys).

## Setup

Enable pipelines in your repository by visiting the Bitbucket GUI `Settings -> Pipelines -> Settings -> Enable`.

### Sentry

All these steps are required:

* Create a Sentry account
* Create an organization
* Add a project
* Attach the Bitbucket Repository to Sentry via https://sentry.io/settings/%SENTRY_ORG%/repos/

### Required Environment Variables

Setup these variables via the Bitbucket GUI `Settings -> Pipelines -> Environment variables`. You can see these variables in the Sentry URL when viewing a project, e.g. https://sentry.io/%SENTRY_ORG%/%SENTRY_PROJECT%/.

* `SENTRY_PROJECT` this is a unique ID used to identify the project.
* `SENTRY_ORG` this is a unique ID used to identify the organization.
* `SENTRY_AUTH_TOKEN` Generated from [here](https://sentry.io/settings/account/api/auth-tokens/new-token/), can keep the default permissions. Mask and encrypt when adding the variable to Bitbucket for security reasons.
